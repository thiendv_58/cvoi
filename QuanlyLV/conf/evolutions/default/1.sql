# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table student (
  id                        bigint not null,
  student_code              varchar(255),
  password                  varchar(255),
  email                     varchar(255),
  name                      varchar(255),
  date                      timestamp,
  address                   varchar(255),
  phone_number              varchar(255),
  faculty                   varchar(255),
  course                    varchar(255),
  additional_infomation     varchar(255),
  picture                   bytea,
  constraint pk_student primary key (id))
;

create table user_account (
  id                        bigint not null,
  email                     varchar(255),
  password                  varchar(255),
  permission                varchar(255),
  constraint pk_user_account primary key (id))
;

create sequence student_seq;

create sequence user_account_seq;




# --- !Downs

drop table if exists student cascade;

drop table if exists user_account cascade;

drop sequence if exists student_seq;

drop sequence if exists user_account_seq;

