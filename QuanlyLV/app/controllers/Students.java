package controllers;

import java.util.Random;

import com.avaje.ebean.Page;

import models.SendEmail;
import models.Student;
import models.UserAccount;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.students.details;
import views.html.students.list;

/**
 * Created by MRTHACKER on 31-Mar-15.
 */

@Security.Authenticated(Secured.class)
public class Students extends Controller {

	private static final Form<Student> studentForm = Form.form(Student.class);

	public static Result list(Integer page) {
		Page<Student> students = Student.find(page);
		return ok(list.render(students));
	}

	public static Result newStudent() {

		return ok(details.render(studentForm));
	}

	public static Result details(Student student) {
		if (student == null) {
			return notFound(String.format("Học viên không tồn tại."));
		}
		if (Student.findByEmail(session().get("email")) != null
				&& !Student.findByEmail(session().get("email")).studentCode
						.equals(student.studentCode)) {
			return redirect(routes.Students.list(0));
		}
		Form<Student> filledForm = studentForm.fill(student);
		return ok(details.render(filledForm));
	}

	public static Result save() {
		Form<Student> boundForm = studentForm.bindFromRequest();
		if (boundForm.hasErrors()) {
			flash("error", "Yêu cầu nhập đầy đủ thông tin");
			return badRequest(details.render(boundForm));
		}
		Student student = boundForm.get();
		if (UserAccount.findByEmail(student.email) != null) {
			flash("error", "Email này không được phép sử dụng");
			return badRequest(details.render(boundForm));
		}
		
		for (int i = 0; i < student.name.length(); i++) {
			// System.out.println(student.name.indexOf(i));
			int code = student.name.codePointAt(i);
			if ((code < 65 || code > 122 || (code > 90 && code < 97))
					&& code <= 126) {
				if (code == 32)
					continue;
				flash("error",
						"Tên học viên không được chứa ký tự đặc biệt và số.");
				return badRequest(details.render(boundForm));
			}
		}
		student.name = student.name.trim();
		if (student.id == null) {
			if (Student.findByEmail(student.email) != null) {
				flash("error", "Email này không được phép sử dụng");
				return badRequest(details.render(boundForm));
			}
			student.password = passRandom();
			student.save();
			SendEmail.sendMail(student.email, student.password);
			 flash("success", String.format(student.password,student));
		} else {
			if (Student.findByEmail(session().get("email")) != null) {
				student.faculty = new String(
						Student.findByEmail(student.email).faculty);
			}
			student.update();
		}

		// flash("success", String.format("Thêm thành công học viên %s",
		// student));
		if(Student.findByEmail(session().get("email"))!=null)
		{
			return redirect(routes.Students.infostudent(Student.findByEmail(session().get("email"))));
		}
		return redirect(routes.Students.list(0));
	}

	public static Result delete(String ean) {
		final Student student = Student.findByEan(ean);
		if (student == null) {
			return notFound(String.format("Học viên %s không tồn tại.", ean));
		}

		student.delete();
		return redirect(routes.Students.list(0));
	}

	public static Result infostudent(Student student) {

		return ok(views.html.students.studentview.render(student));

	}

	public static String passRandom() {
		Random rd = new Random();
		String value = "1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
		String pass = "";
		for (int i = 0; i < 10; i++) {
			pass += value.charAt(rd.nextInt(value.length()));
		}
		return pass;
	}

}
