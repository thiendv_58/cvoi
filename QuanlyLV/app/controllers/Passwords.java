package controllers;

import models.Student;
import play.mvc.Controller;
import play.mvc.Result;
import play.data.Form;
import views.html.students.details;
import static play.data.Form.form;

public class Passwords extends Controller {
	public static class Passwordd {
		public String oldPass;
		public String newPass;
		public String renewPass;
	}

	public static Result changePass() {
		return ok(views.html.password.changepass.render(form(Passwordd.class)));
	}

	public static Result savePass() {
		Form<Passwordd> pass = form(Passwordd.class).bindFromRequest();
		// flash("error",
		// pass.get().oldPass+" "+pass.get().newPass+" "+pass.get().renewPass);
		Student student = Student.findByEmail(session().get("email"));
		String oldPass = pass.get().oldPass;
		String newPass = pass.get().newPass;
		String renewPass = pass.get().renewPass;
		if (oldPass.trim().equals("") || newPass.trim().equals("")
				|| renewPass.trim().equals("")) {
			flash("error", "Yêu cầu nhập đủ thông tin!!!");
			return badRequest(views.html.password.changepass
					.render(form(Passwordd.class)));
		}
		if (!oldPass.equals(student.password)) {
			flash("error", "Mật khẩu hiện tại không đúng");
			return badRequest(views.html.password.changepass
					.render(form(Passwordd.class)));
		}
		if (!newPass.equals(renewPass)) {
			flash("error", "Mật khẩu mới không khớp");
			return badRequest(views.html.password.changepass
					.render(form(Passwordd.class)));
		}

		if (oldPass.equals(renewPass)) {
			flash("error", "Mật khẩu mới không được trùng mật khẩu cũ");
			return badRequest(views.html.password.changepass
					.render(form(Passwordd.class)));
		}

		if (newPass.length() < 6 || newPass.length() > 16) {
			flash("error", "Mật khẩu chỉ được chứa từ 6 đến 16 ký tự");
			return badRequest(views.html.password.changepass
					.render(form(Passwordd.class)));
		}

		student.password = newPass;
		student.update();
		flash("success", "Cập nhật mật khẩu thành công");
		return redirect(routes.Students.infostudent(Student
				.findByEmail(session().get("email"))));
	}
}
