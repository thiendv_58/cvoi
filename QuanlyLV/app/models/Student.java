package models;

import com.avaje.ebean.Page;

import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.mvc.PathBindable;

import javax.persistence.Entity;
import javax.persistence.Id;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by MRTHACKER on 31-Mar-15.
 */
@Entity
public class Student extends Model implements PathBindable<Student> {

	public static ArrayList<String> options() {
		ArrayList<String> options = new ArrayList<String>();
		options.add("Công nghệ thông tin");
		options.add("Điện tử viễn thông");

		options.add("Vật lý kỹ thuật và công nghệ nano");

		options.add("Cơ học kỹ thuật và tự động hóa");

		// options.add("Công nghệ thông tin");
		return options;

	}

	@Id
	public Long id;

	@Constraints.Required
	public String studentCode;
	public String password;
	// @Constraints.Required
	// public String userName;
	//
	// @Constraints.Required
	// public String passWord;
	@Constraints.Required
	public String email;

	@Constraints.Required
	public String name;
	@Constraints.Required
	@Formats.DateTime(pattern = "yyyy-MM-dd")
	public Date date;

	public String address;

	public String phoneNumber;
	@Constraints.Required
	public String faculty;
	@Constraints.Required
	public String course; // nganhf

	public String additionalInfomation;

	public byte[] picture;

	// @Constraints.Required
	// public String ean;
	//
	// @Constraints.Required
	// public String name;
	//
	// public String description;

	public static Finder<Long, Student> find = new Finder<Long, Student>(
			Long.class, Student.class);

	public Student() {
	}

	public Student(
			String studentCode, // String userName, String passWord,
			String name, Date date, String address, String phoneNumber,
			String faculty, String k) {
		super();
		this.studentCode = studentCode;
		// this.userName = userName;
		// this.passWord = passWord;
		this.name = name;
		this.date = date;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.faculty = faculty;
		this.course = k;
	}

	public String toString() {
		return String.format("%s - %s", studentCode, name);
	}

	public static List<Student> findAll() {
		return find.all();
	}

	public static Student findByEan(String ean) {
		return find.where().eq("student_code", ean).findUnique();
	}

	public static Student findByName(String term) {
		return find.where().eq("name", term).findUnique();
	}

	public static Page<Student> find(int page) { // trả về trang thay vì List
		return find.where().orderBy("id asc") // sắp xếp tăng dần theo id
				.findPagingList(10) // quy định kích thước của trang
				.setFetchAhead(false) // có cần lấy tất cả dữ liệu một thể?
				.getPage(page); // lấy trang hiện tại, bắt đầu từ trang 0

	}

	@Override
	public Student bind(String key, String value) {
		return findByEan(value);
	}

	@Override
	public String unbind(String key) {
		return studentCode;
	}

	@Override
	public String javascriptUnbind() {
		return studentCode;
	}

	public static Student findByEmail(String email) {
		return find.where().eq("email", email).findUnique();
	}

	public static Student authenticate(String email, String password) {
		return find.where().eq("email", email).eq("password", password)
				.findUnique();
	}
}
